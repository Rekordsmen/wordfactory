package com.example.wordsfactory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsfactory.databinding.NewPageBinding



class Page_Adapter(
    private val context: Context,
    private val Pages: List<OnePage>
) : RecyclerView.Adapter<PageAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.new_page, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = NewPageBinding.bind(holder.itemView)
        val currentPage = Pages[position]

        binding.viewPage.setImageResource(currentPage.image)
        binding.title.text = currentPage.title
        binding.body.text = currentPage.body
        binding.pag.setImageResource(currentPage.pag)


    }

    override fun getItemCount() = Pages.size
}