package com.example.wordsfactory

data class OnePage(
    val image : Int,
    val title : String,
    val body : String,
    val pag : Int
)
